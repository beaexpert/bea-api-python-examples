import base64
import random
import string
import configparser
import os
import time

import bex_api as bea

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
import cryptography.hazmat.primitives.serialization.pkcs12



""" 
    ***************************************************
    GET THE CONFIG FROM FILE OR DEFINE CONFIG ARRAY
    ***************************************************
"""
__current_path = os.getcwd()
__config=[]
__config_file=__current_path+'/private.config.ini'
if os.path.exists(__config_file):
    __config = configparser.ConfigParser()
    __config.read(__config_file)    
else :
    __config["SOFTWARETOKEN"]["B64"] = "...."
    __config["SOFTWARETOKEN"]["FILE"] = "...."
    __config["SOFTWARETOKEN"]["PWD"] = "...." 
    

__DEBUG__ = True
sw_token = __config["SOFTWARETOKEN"]["FILE"]
sw_pin = __config["SOFTWARETOKEN"]["PWD"]
token_b64 = __config["SOFTWARETOKEN"]["B64"]




# Login
token, safeId, sessionKey = bea.bex_login(sw_token, sw_pin, token_b64)

if __DEBUG__:
    print("token: " + token)
    print("safeId: " + safeId)
    print("sessionKey: " + base64.b64encode(sessionKey).decode('ascii'))


# sendMessage/saveMessage
postboxSafeId = safeId

msg_infos = { 
    "betreff": "PY: Test mit Anhang",
    "aktz_sender": "debug aktz_sender",
    "aktz_rcv": "debug aktz_rcv",
    "msg_text": "This is a simple test message",
    "is_eeb": False,
    "dringend": False,
    "pruefen": False,
    "receivers": ["DE.Justiztest.4559a995-0b93-4f9c-be24-08745f648660.cd91"], # be next GmbH 2022 / beA.expert Testaccount
    "attachments": 
        [
            "01_myText.txt"
        ],
    "is_eeb_response": False,
    "eeb_fremdid": "",
    "eeb_date": "",
    "verfahrensgegenstand": "",
    "eeb_erforderlich": False,
    "eeb_accept": False,
    "xj": True, 
    "nachrichten_typ": "ALLGEMEINE_NACHRICHT"
}

msg_att = {
    "attachments": [
        {
            "name" :  "01_myText.txt",
            "data" : "TXkgdGV4dCAx",
            "att_type": ""
        }
    ]
}


betreff_orig = msg_infos['betreff']


msg_infos['betreff']+=" ** SAVED VIA PY-API"
token, info, messageId_save = bea.bea_save_message(token, postboxSafeId, msg_infos, msg_att, sessionKey)

if __DEBUG__:
    print('main: info (saveMessage):')
    print('messageId_save:'+messageId_save)
    print(info)

time.sleep(2)

msg_infos['betreff']=betreff_orig+" ** SENT VIA PY-API"
token, info, messageId_send = bea.bea_send_message(token, postboxSafeId, msg_infos, msg_att, sessionKey)

if __DEBUG__:
    print('main: info (sendMessage):')
    print('messageId_send:'+messageId_send)
    print(info)

quit()