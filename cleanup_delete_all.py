import base64
import random
from re import M
import string
import configparser
import os
import time

import bex_api

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
import cryptography.hazmat.primitives.serialization.pkcs12

""" 
    ***************************************************
    GET THE CONFIG FROM FILE OR DEFINE CONFIG ARRAY
    ***************************************************
"""

__current_path = os.path.dirname(os.path.abspath(__file__)) #os.getcwd()
__config=[]
__config_file=__current_path+'/private.config.ini'
if os.path.exists(__config_file):
    __config = configparser.ConfigParser()
    __config.read(__config_file)    
else :
    __config["SOFTWARETOKEN"]["B64"] = "...."
    __config["SOFTWARETOKEN"]["FILE"] = "...."
    __config["SOFTWARETOKEN"]["PWD"] = "...." 
    
__DEBUG__ = True
sw_token = __config["SOFTWARETOKEN"]["FILE"]
sw_pin = __config["SOFTWARETOKEN"]["PWD"]
token_b64 = __config["SOFTWARETOKEN"]["B64"]

# Login
token, safeId, sessionKey = bex_api.bex_login(sw_token, sw_pin, token_b64)

if __DEBUG__:
    print("token: " + token)
    print("safeId: " + safeId)
    print("sessionKey: " + base64.b64encode(sessionKey).decode('ascii'))

nbre_deleted_ok=0
nbre_deleted_nok=0

print("get postboxes ...")
token, postboxes = bex_api.bea_get_postboxes(token)

for p in postboxes:
    
    if p.postboxSafeId==safeId: # we allow us to delete our own SafeId-postbox but not DELEGATED postboxes
        print("considering postboxid:"+p.postboxSafeId)
        
        for f in p.folder:
            print("considering folderid:"+f.id)
            token, messages = bex_api.bea_get_folderoverview(token, f.id, sessionKey)

            if not messages is None:
                for m in messages:
                    print("considering messageid:"+m.messageId+" to be deleted ...")
                    token, info = bex_api.bea_delete_message(token,m.messageId)
                    print(" > result:"+info)
                    if info=="true":
                        nbre_deleted_ok+=1
                    else:
                        nbre_deleted_nok+=1
                    #time.sleep(1) # we do not want to DDOS the beA!
            else:
                print("folder is empty!")

    else:
        print("skip "+p.postboxSafeId+", which is not our own postbox!")

print("SUMMARY OF OUR BLOODBATH:")
print("Number of messages deleted (confirmed with 'true'):"+str(nbre_deleted_ok))
print("Number of deleting attempts (unfortunatly not confirmed):"+str(nbre_deleted_nok))

quit()
