import base64
import random
import string
import configparser
import os

import bex_api

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
import cryptography.hazmat.primitives.serialization.pkcs12
 

""" 
    ***************************************************
    GET THE CONFIG FROM FILE OR DEFINE CONFIG ARRAY
    ***************************************************
"""
__current_path = os.path.dirname(os.path.abspath(__file__)) #os.getcwd()
__config=[]
__config_file=__current_path+'/private.config.ini'
if os.path.exists(__config_file):
    __config = configparser.ConfigParser()
    __config.read(__config_file)    
else :
    __config["SOFTWARETOKEN"]["B64"] = "...."
    __config["SOFTWARETOKEN"]["FILE"] = "...."
    __config["SOFTWARETOKEN"]["PWD"] = "...." 
    

__DEBUG__ = True
sw_token = __config["SOFTWARETOKEN"]["FILE"]
sw_pin = __config["SOFTWARETOKEN"]["PWD"]
token_b64 = __config["SOFTWARETOKEN"]["B64"]


# Login
token, safeId, sessionKey = bex_api.bex_login(sw_token, sw_pin, token_b64)

if __DEBUG__:
    print("token: " + token)
    print("safeId: " + safeId)
    print("sessionKey: " + base64.b64encode(sessionKey).decode('ascii'))

# sendMessage/saveMessage
postboxSafeId = safeId

msg_infos = { 
    "betreff": "WIN PY: Test mit Anhang",
    "aktz_sender": "debug",
    "aktz_rcv": "debug",
    "msg_text": "This is a simple test message",
    "is_eeb": False,
    "dringend": False,
    "pruefen": False,
    "receivers": ["DE.BRAK_SPT .... INSERT SAFE ID"],
    "attachments": 
        [
            "01_myText.txt"
        ],
    "is_eeb_response": False,
    "eeb_fremdid": "",
    "eeb_date": "",
    "verfahrensgegenstand": "",
    "eeb_erforderlich": True,
    "eeb_accept": False,
    "xj": True, 
    "nachrichten_typ": "ALLGEMEINE_NACHRICHT"
}

msg_att = {
    "attachments": [
        {
            "name" :  "01_myText.txt",
            "data" : "TXkgdGV4dCAx",
            "att_type": ""
        }                                    
    ]
}

info = ''
token, info, messageId = bex_api.bea_save_message(token, postboxSafeId, msg_infos, msg_att, sessionKey)
if __DEBUG__:
    print('info saveMessage:')
    print(info)
    print("messageId: ", messageId)


message_draft, msg_infos, msg_att = bex_api.bea_init_message_draft(token, messageId, sessionKey)
if __DEBUG__:
    print('bea_init_message_draft:')
    print("message_draft: ", message_draft)
    print("msg_infos: ", msg_infos)
    print("msg_att: ", msg_att)


#remove xj
msg_infos["attachments"].remove("xjustiz_nachricht.xml")

for a in msg_att["attachments"]:
    if(a["name"] == "xjustiz_nachricht.xml"):
        msg_att["attachments"].remove(a)
        break


# append a new attachment
your_file="xyz YPOUR FILE HERE TO ADD"
your_file_fullpath=os.path.join(__current_path,your_file)

my_att_bin = b''
f = open(your_file_fullpath, "rb")
my_att_bin=f.read()

msg_infos["attachments"].append(your_file)

att_tmp = {
    "name" : your_file,
    "data" : base64.b64encode(my_att_bin).decode('ascii'),
    "att_type": ""
}  
msg_att["attachments"].append(att_tmp)

token, info, messageId = bex_api.bea_save_message(token, postboxSafeId, msg_infos, msg_att, sessionKey, message_draft)
if __DEBUG__:
    print('info saveMessage:')
    print(info)
    print("messageId: ", messageId)
    

